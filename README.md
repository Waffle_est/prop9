# Proposition 9 tester

A utility program to help investigate the properties of certain unary cyclic regular languages.

This project is a utility created for the purposes of the Bachelors thesis: "Unaarsete tsükliliste regulaarsete keelte pettehulkade eksperimentaalne uurimine"
("Experimental Study of Fooling Sets of Unary Cyclic Regular Languages") TalTech 2021

## Usage:
When run the program will print to stdout the set of UCDFAs for which prop9 does not hold
and a maximal fooling set for the language accepted by the UCDFA.

By default the UCDFAs are all of size 10, which is the smallest size for which this utility can find such examples.

Run in the root directory of this project (fsm_gen). Replace "python" with the appropriate python 3 interpreter.
Python 3.6 or later is needed. (Python 3.9 has been tested most thoroughly)
```cmd
python fsm
python fsm --min 10 --max 10
```
