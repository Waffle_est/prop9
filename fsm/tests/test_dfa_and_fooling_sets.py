import pytest
from itertools import chain

from fooling_set import gen_fs_max_len_only
from fsm import DFA, generate_dfa, generate_dfa_minimal

FSM_GEN_MINIMAL = tuple(generate_dfa_minimal(1, 4))
FSM_GEN = tuple(generate_dfa(1, 4))

# a set of DFA's that are constructed by hand and known to be minimal.
FSM_KNOWN_MINIMAL = [
    DFA(1, {0}),
    DFA(2, {0}),
    DFA(2, {1}),

    DFA(3, {0}),
    DFA(3, {1}),
    DFA(3, {2}),
]

FSM_KNOWN_NOT_MINIMAL = [
    DFA(2, {0, 1}),
    DFA(6, {1, 3, 5}),
    DFA(6, set(range(6))),
]

FS_CANDIDATE_PROVIDERS = [
    gen_fs_max_len_only,
]


@pytest.mark.parametrize(
    'fsm, test, accept',
    [
        # fsm with lone accepting state will forever accept all states
        (DFA(N=1, F={0}), 0, True),
        (DFA(N=1, F={0}), 1, True),
        (DFA(N=1, F={0}), 2, True),

        # fsm with 2 states, the first of which is accepting and ending in a single-state-cycle will accept
        #    empty strings and nothing else
        (DFA(N=2, F={0}), 0, True),
        (DFA(N=2, F={0}), 1, False),
        (DFA(N=2, F={0}), 2, True),
        (DFA(N=2, F={0}), 3, False),

        (DFA(N=3, F={0}), 0, True),
        (DFA(N=3, F={0}), 1, False),
        (DFA(N=3, F={0}), 2, False),
        (DFA(N=3, F={0}), 3, True),
        (DFA(N=3, F={0}), 4, False),
        (DFA(N=3, F={0}), 5, False),
        (DFA(N=3, F={0}), 6, True),

    ]
)
def test_fsm_string_acceptance_correctness(fsm: DFA, test, accept: bool):
    assert fsm.test_word(test) == accept


def test_fsm_eq():
    assert DFA(N=2, F={1}) == DFA(N=2, F={1})


@pytest.mark.parametrize('fsm1,fsm2', [
    (DFA(N=2, F={1}), DFA(N=2, F={1})),
    (DFA(N=5, F={1, 3}), DFA(N=5, F={1, 3})),
])
def test_fsm_hash_eq(fsm1, fsm2):
    assert hash(fsm1) == hash(fsm2)


@pytest.mark.parametrize('n', [1, 2, 4])
def test_gen_fsm_uniqueness(n):
    """
        ensure that the fsm generator does not emit duplicate cases
        (the set constructor discards tupleicates, thus a difference in the number of contained elements would imply duplicates.)
    """
    fsms1 = tuple(generate_dfa(n, n))
    fsms2 = set(fsms1)
    assert len(fsms1) == len(fsms2)


@pytest.mark.parametrize('fsm', FSM_GEN_MINIMAL)
def test_gen_transition_table_completeness(fsm):
    """
        in our specific case there should be exactly one transition out of every single state.
        the transition table format does not support cases with more than 1 transition thus
        as long as the size of the transition table is equal to the size of the state set there are no missing entries.
    """
    assert len(fsm.Q) == len(fsm.delta)


@pytest.mark.parametrize('fsm', FSM_GEN_MINIMAL)
def test_gen_fsm_minimal_only_produces_minimal_DFA(fsm):
    assert fsm.is_minimal()


@pytest.mark.parametrize('fsm', FSM_GEN)
@pytest.mark.parametrize('fs_candidate_provider', FS_CANDIDATE_PROVIDERS)
def test_validate_no_illegal_fooling_sets(fsm: DFA, fs_candidate_provider):
    fs = fsm.get_fooling_set(extended=False, candidates=fs_candidate_provider)
    fs.validate(fsm, extended=False)


@pytest.mark.parametrize('fsm', FSM_GEN)
@pytest.mark.parametrize('fs_candidate_provider', FS_CANDIDATE_PROVIDERS)
def test_validate_no_illegal_ext_fooling_sets(fsm: DFA, fs_candidate_provider):
    fs = fsm.get_fooling_set(extended=True, candidates=fs_candidate_provider)
    fs.validate(fsm, extended=True)


@pytest.mark.parametrize('fsm', FSM_GEN)
def test_fooling_set_size_does_not_exceed_DFA_states(fsm: DFA):
    fooling_set = fsm.get_fooling_set()
    assert len(fooling_set) <= fsm.N


@pytest.mark.parametrize('fsm', FSM_GEN_MINIMAL)
def test_fooling_set_size_does_not_exceed_DFA_states_2(fsm: DFA):
    fooling_set = fsm.get_fooling_set()
    assert len(fooling_set) <= fsm.N


@pytest.mark.parametrize('fsm', FSM_GEN)
def test_prop9_fooling_set_size_does_not_exceed_DFA_states(fsm: DFA):
    fooling_set = fsm.get_fooling_set_from_prop9_constraints()
    assert len(fooling_set) <= fsm.N


@pytest.mark.parametrize('fsm', FSM_GEN_MINIMAL)
def test_prop9_fooling_set_size_does_not_exceed_DFA_states_2(fsm: DFA):
    fooling_set = fsm.get_fooling_set_from_prop9_constraints()
    assert len(fooling_set) <= fsm.N


@pytest.mark.parametrize('fsm', FSM_GEN)
def test_prop9_fooling_set_is_tight_if_prop9_holds(fsm: DFA):
    fooling_set = fsm.get_fooling_set_from_prop9_constraints()
    if fsm.prop9():
        assert len(fooling_set) == fsm.N
    else:
        assert len(fooling_set) == 0


def test_fsm_gen_min_completeness():
    wide_set = frozenset(FSM_GEN)
    narrow_set = frozenset(FSM_GEN_MINIMAL)
    elements_not_in_narrow_set = wide_set - narrow_set
    for e in elements_not_in_narrow_set:
        assert not e.is_minimal()


def test_fsm_gen_min_completeness_by_prop9():
    wide_set = frozenset(FSM_GEN)
    narrow_set = frozenset(FSM_GEN_MINIMAL)
    elements_not_in_narrow_set = wide_set - narrow_set
    for e in elements_not_in_narrow_set:
        assert not e.prop9()


@pytest.mark.parametrize('fsm', FSM_KNOWN_MINIMAL)
def test_minimize_by_reversal_minimal_DFA_yeields_identical_dfa(fsm):
    minimal = fsm.minimize()
    assert fsm == minimal


@pytest.mark.parametrize('fsm', FSM_KNOWN_NOT_MINIMAL)
def test_minimize_by_reversal_non_minimal_dfa_yields_smaller_dfa(fsm):
    minimal = fsm.minimize()
    assert fsm != minimal
    assert minimal.N < fsm.N


@pytest.mark.parametrize('fsm', FSM_KNOWN_MINIMAL)
def test_is_minimal_correctness_True(fsm):
    assert fsm.is_minimal()


@pytest.mark.parametrize('fsm', FSM_KNOWN_NOT_MINIMAL)
def test_is_minimal_correctness_False(fsm):
    assert not fsm.is_minimal()


@pytest.mark.parametrize('fsm,complement', [
    (DFA(1, {0}), DFA(1, set({}))),
    (DFA(3, {0, 2}), DFA(3, {1})),
    (DFA(3, {0}), DFA(3, {1, 2})),
])
def test_complement_correctness(fsm: DFA, complement: DFA):
    assert fsm.complement() == complement


@pytest.mark.parametrize('fsm', FSM_GEN_MINIMAL)
# @pytest.mark.parametrize('fsm', generate_dfa_minimal(1,15))
def test_complement_of_minimal_dfa_is_minimal(fsm: DFA):
    assert fsm.complement().is_minimal()


@pytest.mark.parametrize('fsm', set(FSM_GEN) - set(FSM_GEN_MINIMAL))
# @pytest.mark.parametrize('fsm', set(generate_dfa(1, 17)) - set(generate_dfa_minimal(1, 17)))
def test_complement_minimization(fsm: DFA):
    """ This test asserts that minimising a dfa is equivalent to complementing,
        minimising the complement, then complementing again.
    """
    assert not fsm.is_minimal()
    complement = fsm.complement()
    assert not complement.is_minimal()
    min1 = fsm.minimize()
    min_complement = complement.minimize()
    min2 = min_complement.complement()
    assert min1 == min2
