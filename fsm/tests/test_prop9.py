"""
    Tests that are of interest theory wise
"""

import pytest

from fsm import DFA, generate_dfa, generate_dfa_minimal
from tests.test_dfa_and_fooling_sets import FSM_GEN_MINIMAL, FSM_GEN


@pytest.mark.parametrize('fsm', FSM_GEN)
def test_prop9_implies_minimality(fsm: DFA):
    if fsm.prop9():
        assert fsm.is_minimal()


@pytest.mark.xfail
@pytest.mark.parametrize('fsm', FSM_GEN)
def test_minimality_implies_prop9(fsm: DFA):
    """ DFA minimality does not imply the dfa adheres to prop9 constraints.
    """
    if fsm.is_minimal():
        assert fsm.prop9()


@pytest.mark.parametrize('fsm', FSM_GEN_MINIMAL)
def test_exists_fooling_set_that_exceeds_prop9_size(fsm: DFA):
    # implied precondition: fsm.is_minimal()
    if not fsm.prop9():
        fs = fsm.get_fooling_set(extended=False)
        assert len(fs) < fsm.N


@pytest.mark.parametrize('fsm', FSM_GEN_MINIMAL)
def test_exists_ext_fooling_set_that_exceeds_prop9_size(fsm: DFA):
    # implied precondition: fsm.is_minimal()
    if not fsm.prop9():
        fs = fsm.get_fooling_set(extended=True)
        assert len(fs) < fsm.N


@pytest.mark.xfail()
@pytest.mark.parametrize('fsm', FSM_GEN_MINIMAL)
def test_fooling_set_or_fooling_set_of_complement_is_tight(fsm: DFA):
    fsm2 = fsm.complement()
    fs1 = fsm.get_fooling_set()
    fs2 = fsm2.get_fooling_set()
    assert (fsm.N == len(fs1)) or (fsm.N == len(fs2))


@pytest.mark.parametrize('fsm', generate_dfa_minimal(1, 7))
def test_extended_fooling_set_or_extended_fooling_set_of_complement_is_tight_for_small_dfa_s7(fsm: DFA):
    """ either the extended fooling set of a minimal dfa,
    or the extended fooling set of the complement of the minimal dfa is tight for dfa size no greater than 7

    """
    fsm2 = fsm.complement()
    fs1 = fsm.get_fooling_set(extended=True)
    fs2 = fsm2.get_fooling_set(extended=True)
    assert (fsm.N == len(fs1)) or (fsm.N == len(fs2))


@pytest.mark.xfail
@pytest.mark.parametrize('fsm', [
    DFA(8, {0, 2, 4}),
    DFA(8, {0, 2, 6}),
    DFA(8, {0, 4, 6}),
    DFA(8, {1, 3, 5}),
    DFA(8, {1, 3, 7}),
    DFA(8, {1, 5, 7}),
])
# @pytest.mark.parametrize('fsm', generate_dfa_minimal(1,8))
def test_extended_fooling_set_or_extended_fooling_set_of_complement_is_tight(fsm: DFA):
    """
        There are many examples where neither the extended fooling set of the dfa not the extended fooling set of the complement are tight. at dfa size 8 or above.
    :param fsm:
    :return:
    """
    fsm2 = fsm.complement()
    fs1 = fsm.get_fooling_set(extended=True)
    fs2 = fsm2.get_fooling_set(extended=True)
    assert (fsm.N == len(fs1)) or (fsm.N == len(fs2))


@pytest.mark.xfail
@pytest.mark.parametrize('fsm', FSM_GEN_MINIMAL)
def test_get_fooling_set_fast_is_equivalent_to_get_fooling_set(fsm: DFA):
    assert len(fsm.get_fooling_set()) == len(fsm.get_fooling_set_using_1_final_state())


@pytest.mark.xfail
@pytest.mark.parametrize('fsm', FSM_GEN_MINIMAL)
def test_get_fooling_set_fast_is_equivalent_to_get_fooling_set_ext(fsm: DFA):
    assert len(fsm.get_fooling_set(extended=True)) == len(fsm.get_fooling_set_using_1_final_state(extended=True))
