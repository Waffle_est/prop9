from typing import Sequence, Iterable, Tuple
from itertools import product


class FoolingSet:
    def __init__(self, X: Sequence[int], Y: Sequence[int]):
        assert len(X) == len(Y)
        assert len(X) == len(set(X))
        assert len(Y) == len(set(Y))
        self.X = X
        self.Y = Y

    def __len__(self):
        return len(self.X)

    def __str__(self):
        return (
            'FS{' +
            ', '.join(f'({x}, {y})' for x, y in iter(self))
            + '}'
        )

    def __iter__(self):
        return zip(self.X, self.Y)

    __repr__ = __str__

    def validate_as_extended_fooling_set_of(self, fsm: 'DFA'):
        """Assert that this fooling set is a valid extended fooling set of fsm

        :fsm: The DFA to validate this fooling set against.
        """
        # ensure fooling set constraints in relation to pars forming valid words of the fsm language.
        for i in range(len(self)):
            for j in range(len(self)):
                if i == j:
                    x = self.X[i]
                    y = self.Y[j]
                    assert fsm.test_word(n_elements=(x + y))
                if i != j:
                    xiyj = self.X[i] + self.Y[j]
                    xjyi = self.X[j] + self.Y[i]
                    assert (
                            (not fsm.test_word(n_elements=xiyj))
                            or
                            (not fsm.test_word(n_elements=xjyi))
                    )

    def validate_as_fooling_set_of(self, fsm: 'DFA'):
        """Assert that this fooling set is a valid fooling set of fsm

        :fsm: The DFA to validate this fooling set against.
        """
        for i, x in enumerate(self.X):
            for j, y in enumerate(self.Y):
                if i == j:
                    assert fsm.test_word(n_elements=(x + y))
                if i != j:
                    assert not fsm.test_word(n_elements=(x + y))

    def validate(self, fsm, extended: bool = True):
        """ Test this fooling set against a particular DFA and ensure, that it is a valid fooling set for that automata.
        a heavy and reliable validation routine.
        :return:
        """
        # ensure basic integrity of fooling set

        # Following 3 statements validated in fooling set constructor:
        # assert len(fs.X) == len(set(fs.X))
        # assert len(fs.Y) == len(set(fs.Y))
        # assert len(fs.X) == len(fs.Y)

        # ensure fooling set constraints in relation to pairs forming valid words of the fsm language.
        if extended:
            return self.validate_as_extended_fooling_set_of(fsm)
        else:
            return self.validate_as_fooling_set_of(fsm)


def is_valid_extended_fs(left, right, final_states, n):
    """
        Tests if the ordered sets  left and right form a valid extended fooling set for
        a UCDFA described by the given set of final states and total number of states
    """
    for i in range(1, len(left)):
        for j in range(i):
            xiyj = (left[i] + right[j]) % n
            xjyi = (left[j] + right[i]) % n
            if xiyj in final_states and xjyi in final_states:
                return False

    return True

def gen_fs_max_len_only(n, final_states):
    left = tuple(range(n))
    # A helper lookup table. This helps to generate only promising pairings.
    candidate_offsets = {
        x: tuple((f - x + n) % n for f in final_states)
        for x in left
    }

    def generate_candidate_pairings(cursor=0, current_candidate=None):
        if current_candidate is None:
            current_candidate = []
        if cursor == n:
            if is_valid_extended_fs(left, current_candidate, final_states, n):
                yield tuple(current_candidate)
            return

        x = left[cursor]
        for offset in candidate_offsets[x]:
            if offset not in current_candidate:
                current_candidate.append(offset)
                yield from generate_candidate_pairings(cursor + 1, current_candidate)
                current_candidate.pop()

    for right in generate_candidate_pairings():
        yield left, right
