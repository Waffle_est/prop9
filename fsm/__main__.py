from dfa import generate_dfa_minimal
from argparse import ArgumentParser, ArgumentTypeError


def test_prop9_inverse(min_n=10, max_n=10):
    for n in range(min_n, max_n + 1):

        print(f"testing DFA's of size {n}: [", end="")
        for i, dfa in enumerate(generate_dfa_minimal(n, n)):
            print_progress(i)

            if dfa.prop9():
                continue

            fs = dfa.get_maximum_fooling_set()
            if fs is None or len(fs) != dfa.N:
                continue

            fs.validate(dfa)  # a more robust test for FoolingSet validity then the generator one, just in case.

            print()
            print(repr(dfa), "    ", str(dfa), "    ", fs, flush=True)

        print("]")

    print("--DONE--")


def print_progress(i):
    if i % 5 == 0:
        # the search can take a while so let's print some dots to signal that we are still making progress.
        print(".", end="", flush=True)


def main(min_n: int, max_n: int):
    # 10 is the first instance we see a counter example
    test_prop9_inverse(min_n, max_n)


def validate_min_max(arg):
    value = int(arg)
    if value <= 0:
        raise ArgumentTypeError(f"{arg} needs to be >= 1")
    return value


if __name__ == '__main__':
    parser = ArgumentParser(description="Prop 9 tester")
    parser.add_argument("--min", dest="min", type=validate_min_max, default=10,
                        help="A positive integer, 1 <= --min <= --max")
    parser.add_argument("--max", dest="max", type=validate_min_max, default=10,
                        help="A positive integer, --min <= --max")
    args = parser.parse_args()
    main(min_n=args.min, max_n=args.max)
