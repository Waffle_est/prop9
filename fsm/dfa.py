from typing import List, Tuple, Set, Iterable, Dict, FrozenSet, Union
from itertools import combinations
from fooling_set import FoolingSet, gen_fs_max_len_only


class DFA:
    '''
        A unary cyclic DFA should be well enough defined if we know the number of states,
        set of final/accepting states and the initial state. the initial state is
        presumed to be q0, the transition function to be advancing to the state with an
        index 1 greater than the current state for the lone possible input (modulo number of states).

        We are only interested in minimal DFAs

    '''

    def __init__(self, N: int, F: Set[int]):
        """
            Constructs a unary cyclic finite state machine with Q = {qi for i in range(0, N-1)} being the states
             and F being the set of final states encoded as the indicies of corresponding states in Q.
        """
        self.N = N
        self.F = F

    def test_word(self, n_elements: int = None, word: str = None) -> bool:
        ''' Does this FSM accept the given inout?

            assumes correct input (correct number of elements of correct alphabet.
        :param n_elements: pretend you feed in n copies of the single possible element
        :param word: or give the input as explicit repetitions of the input element.
        :return: true if the word is in the language, false otherwise.
        '''
        # TODO: give correct results on nonsense input
        if n_elements is not None:
            pass
        elif word is not None:
            if len(set(word)) <= 1:
                n_elements = len(word)
            else:
                raise ValueError("tested word is not on a unary alphabet.")
        else:
            raise ValueError("unexpected word to test")

        # For this logic it is important that we are working with regular languages,
        # where the alphabet contains exactly one symbol.
        return (n_elements % self.N) in self.F

    def minimize(self) -> 'DFA':
        """
            Reverse the current dfa, then turn produced NFA back into a DFA.
            The algorithm is based on Brozozowskis method of DFA minimization by double reversal.
            Due to the properties of a unary cyclic DFA reversing the dfa  once is sufficient. (not by accident)
        :return : A DFA that is equivalent to this dfa but is minimal.
        """

        def get_previous_state(q: int) -> int:
            """Semantically this is traversing the transition function in the reverse direction, but the
             transition function is implicit in the structure of the unary cyclic DFA.

             :q: The index of the state in the DFA.
             :return: The index of the only possible immediately preceding state.
            """
            return q - 1 if q > 0 else originalN - 1

        def contains_original_starting_state(q):
            """q contains the original q0?"""
            return 0 in q

        # the number of states in the original DFA. This information implies the set of states Q for the original DFA
        # as well as the length of the cycle that the states of the DFA form.
        originalN = self.N
        cursor = frozenset(self.F)

        # While constructing the new DFA each state in the new DFA will be named by a set of states in the original DFA.
        Q: List[FrozenSet[int]] = []

        # Iterate until the next transition is to a state that already exists.
        # The list membership test is O(n), but we are dealing with very small lists.
        while cursor not in Q:
            Q.append(cursor)
            cursor = frozenset(get_previous_state(q) for q in cursor)

        size_of_new_automata = len(Q)

        # generate new terminal states by finding states that contain the original starting state.
        new_set_of_final_states = {index_of_new_final_state for index_of_new_final_state, q in enumerate(Q)
                                   if contains_original_starting_state(q)}

        # TODO: explain why we can just construct a new DFA with the identical cyclic structure.
        #       This needs to be addressed in the thesis paper.
        return DFA(N=size_of_new_automata, F=new_set_of_final_states)

    def is_minimal(self) -> bool:
        '''
            attempt to minimize this dfa, if derived dfa is smaller then this dfa then this dfa must not be minimal.
        :return: True if this DFA is minimal, False otherwise
        '''
        return self == self.minimize()

    def prop9(self) -> bool:
        '''
            Test if this DFA meets the proposition 9 constraints.
            implies existence of a fooling set constructed using only 1 final state of size len(Q)
        '''
        n = self.N
        for i in self.F:
            # qj second state used to compute probing location
            for j in self.F:
                if i == j: continue
                # adding self.N to make sure the intermediate is a positive number even if j>2i
                probe = ((2 * i) - j + n) % n
                if probe in self.F:
                    break
            else:  # probe did not find another final state for the entirety of the innermost loop.
                return True
        return False

    @property
    def delta(self) -> Dict[int, int]:
        """Materializes a mapping that represents the transition function for this finite state machine.

        This mapping is implicit in the structure of the FSM, but it can be useful to generate a more familiar representation.
        """
        loop_start = 0
        _delta = {i: i + 1 for i in range(self.N - 1)}
        _delta[self.N - 1] = loop_start
        return _delta

    @property
    def Q(self) -> Tuple[int]:
        """The indicies of the states that make up this finite state machine."""
        return tuple(range(self.N))

    def __str__(self):
        def _format_state_set(states):
            return '{' + ', '.join('q' + str(q) for q in states) + '}'

        Q = _format_state_set(self.Q)
        F = _format_state_set(self.F)
        delta = str(self.delta)
        return f'DFA(Q={Q}, Σ={{a}}, δ={delta}, I=q0, F={F})'

    def __repr__(self):
        return f'{self.__class__.__name__}({self.N}, {self.F})'

    def __eq__(self, other):
        return (self is other) or (
                self.N == other.N
                and set(self.F) == set(other.F)
        )

    def __hash__(self):
        # self.Q is a generated property from N, thus Q need not be part of the hash.
        F = self.F
        if not isinstance(F, tuple):
            F = tuple(sorted(F))

        return hash((self.N, F))

    @staticmethod
    def _validate_fs_candidate(X, Y, F, N):
        # assert len(X) == len(Y)
        if len(set(Y)) < len(Y):
            return False

        for i in range(1, len(X)):
            for j in range(i):
                xiyj = (X[i] + Y[j]) % N
                xjyi = (X[j] + Y[i]) % N
                if xiyj in F or xjyi in F:
                    return False

        return True

    @staticmethod
    def _validate_fs_ext_candidate(X, Y, F, N):
        # assert len(X) == len(Y)
        if len(set(Y)) < len(Y):
            return False

        for i in range(1, len(X)):
            for j in range(i):
                xiyj = (X[i] + Y[j]) % N
                xjyi = (X[j] + Y[i]) % N
                if xiyj in F and xjyi in F:
                    return False

        return True

    def get_fooling_set_from_prop9_constraints(self) -> FoolingSet:
        """
            Prop9 describes a subset of possible fooling sets.
            This subset is constructed by creating pairs of the indicies of all the states
            and offsets between one particular final state and the corresponding indicies.

            if the DFA passes the prop9 test then at least one such fooling set should exist.
            :return:
        """
        n = self.N
        for i in self.F:
            # qj second state used to compute probing location
            for j in self.F:
                if i == j: continue
                # adding self.N to make sure the intermediate is a positive number even if j>2i
                probe = ((2 * i) - j + n) % n
                if probe in self.F:
                    break
            else:  # probe did not find another final state for the entirety of the innermost loop.
                f = i
                X = self.Q
                Y = ((f - x + n) % n for x in X)
                return FoolingSet(tuple(X), tuple(Y))

        return FoolingSet(X=(), Y=())

    def get_fooling_set(self, extended=False, candidates=gen_fs_max_len_only) -> FoolingSet:
        """

        """
        F = self.F
        N = self.N

        _validate = self._validate_fs_ext_candidate if extended else self._validate_fs_candidate

        for X, Y in candidates(N, F):
            if _validate(X, Y, F, N):
                return FoolingSet(X, Y)

        # making it to this line means no non-empty fooling set was found.
        return FoolingSet(X=tuple(), Y=tuple())

    def get_maximum_fooling_set(self) -> Union[FoolingSet, None]:
        """
            If the DFA has a fooling set of size n, return such a fooling set, otherwise return
            a fooling set of size 0.
            :return : A FoolingSet of size self.N or None
        """
        for left, right in gen_fs_max_len_only(n=self.N, final_states=self.F):
            return FoolingSet(left, right)

        # making it to this line means that we could not find a fooling set of size n.
        return None

    def complement(self) -> 'DFA':
        """generates a DFA which is the complement of this DFA"""
        F = set(self.Q) - set(self.F)
        return DFA(self.N, F)


def _final_state_candidates(number_of_states):
    '''
        Creates a list of candidate sets of final states for a DFA of size Q
        :param Q: The number of states in the to be created DFA
    '''
    number_of_final_states = (number_of_states + 1) // 2
    for q in range(1, number_of_final_states + 1):
        yield from combinations(range(number_of_states), q)


def generate_dfa_minimal(minQ: int, maxQ: int) -> Iterable[DFA]:
    '''
        Only generates DFAs whose states constituta a single big loop
        Only generates minimal DFAs
    '''

    for number_of_states in range(minQ, maxQ + 1):
        for F in _final_state_candidates(number_of_states):
            dfa = DFA(number_of_states, F)
            if dfa.is_minimal():
                yield dfa


def generate_dfa(Qmin: int, Qmax: int) -> Iterable[DFA]:
    '''only generates DFAs whose states constituta a single big loop'''
    # TODO: minimize number of generated FSMs try to avoid equivalent ones.
    for number_of_states in range(Qmin, Qmax + 1):
        for F in _final_state_candidates(number_of_states):
            yield DFA(number_of_states, F)
